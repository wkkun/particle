package  com.wkkun.particle.bubble

import android.content.Context
import android.graphics.*
import com.wkkun.particle.base.BaseItem
import com.wkkun.particle.R
import com.wkkun.particle.util.ScreenUtil
import kotlin.math.max
import kotlin.random.Random

/**
 *Date:2020/4/9
 *Author:wang kunkun
 *Des:
 *
 * 气泡是从父View的地步向上浮动
 **/
class BubbleItem(private val parentWidth: Int, private val parentHeight: Int,context: Context) :
    BaseItem {

    companion object {
        const val STATE_LIVE = 1
        const val STATE_DIE = 0
    }
    private val baseBubbleRadius = ScreenUtil.dip2px(context,2f)
    private val intervalBubbleRadius = ScreenUtil.dip2px(context,3f)

    //起点
    private var origX: Float = 0f
    private var origY: Float = parentHeight.toFloat()

    //终点
    private var desY: Float = 0f
    //当前的位置
    private var curX = 0f
    private var curY = 0f

    //每次刷新 在Y轴的偏移量
    private var speedY = ScreenUtil.dip2px(context,2f)
    private val baseSpeedX =ScreenUtil.dip2px(context,0.5f)
    //每次刷新 在X轴的偏移量
    private var speedX = 0f

    var radius = 20f
    //透明的距离
    private var alphaDis = 0f
    var state = STATE_DIE
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var drawBitmap: Bitmap? = null
    private var resRect: Rect? = null

    init {
        paint.style = Paint.Style.FILL_AND_STROKE
        paint.color = Color.BLUE
    }

    /**
     * 初始化 随机生成气泡的出生地点
     */
    override fun init(context: Context) {
        //获取气泡的bitmap
        if (drawBitmap == null) {
            drawBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.bubble)
            resRect = Rect(0, 0, drawBitmap?.width ?: 0, drawBitmap?.height ?: 0)
        }
        origX = Random.nextInt(100, parentWidth - 100).toFloat()
        desY = 2 * parentHeight / 3 - Random.nextInt(0, parentHeight / 2).toFloat()
        alphaDis = (origY - desY) * 0.2f
        radius = Random.nextFloat() * intervalBubbleRadius + baseBubbleRadius
        speedX = baseSpeedX * Random.nextFloat() * if (Random.nextBoolean()) {
            1
        } else {
            -1
        }
        curX = origX
        curY = origY
        state = STATE_LIVE
        //在边界处的粒子 没有横向速度
        if (curX <= 200 || curX > (parentWidth - 200)) {
            speedX = 0f
        }
        paint.alpha = 255
    }

    override fun preInit(context: Context) {
        //起点的X轴坐标
        init(context)
        curY = desY + max((origY - desY) * Random.nextFloat(), 0f)
    }

    override fun move(): Boolean {
        curY -= speedY
        curX += speedX
        val diff = curY - desY
        if (diff <= alphaDis) {
            if (diff <= alphaDis * 0.4 && diff >=0.3 * alphaDis) {
                paint.alpha = 255
            } else {
                //开始透明
                paint.alpha = (255 * diff / alphaDis + 0.5f).toInt()
            }
        }
        if (curY < desY) {
            state = STATE_DIE
            return false
        }
        if (curX <= 20 || curX >= parentWidth - 20) {
            state = STATE_DIE
            return false
        }
        return true
    }


    override fun reset() {

    }

    override fun draw(canvas: Canvas) {
        drawBitmap?.apply {
            if (!isRecycled) {
                canvas.drawBitmap(this, resRect, RectF(curX - radius, curY - radius, curX + radius, curY + radius), paint)
            }
        }

    }

    override fun isLive(): Boolean {
        return state == STATE_LIVE
    }


    override fun destroy() {
        drawBitmap?.recycle()
        drawBitmap = null
    }
}