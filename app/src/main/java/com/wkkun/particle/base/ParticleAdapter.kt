package com.wkkun.particle.base

/**
 *Date:2020/5/8
 *Author:wang kunkun
 *Des:
 **/
abstract class ParticleAdapter {
    abstract fun newItem(parentWidth: Int, parentHeight: Int): BaseItem
    abstract fun preCreateCount(): Int
    fun maxParticleCount(): Int {
        return Int.MAX_VALUE
    }
}