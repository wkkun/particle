package  com.wkkun.particle.base

import android.content.Context
import android.graphics.Canvas

/**
 *Date:2020/4/9
 *Author:wang kunkun
 *Des:
 * 粒子
 **/
interface BaseItem {

    /**
     * 初始化View
     * 初始化:出生的位置 移动的速度 移动的距离 移动的方向
     */
    fun init(context: Context)

    /**
     * 提前生成粒子的初始化
     */
    fun preInit(context: Context)

    /**
     * 移动View
     *@return true 还能移动 false 消亡
     */
    fun move():Boolean

    /**
     * 重置
     */
    fun reset()

    /**
     * 绘制View
     */
    fun draw(canvas: Canvas)

    /**
     * 当前粒子是否存活
     */
    fun isLive():Boolean

    /**
     * 粒子消亡
     */
    fun destroy()
}