package com.wkkun.particle.base

import android.graphics.Canvas

/**
 *Date:2020/5/8
 *Author:wang kunkun
 *Des:粒子接口
 **/
interface BaseParticle {

    /**
     * 提前生成粒子
     */
    fun preCreate()

    /**
     * 获取粒子
     */
    fun getItem(): BaseItem

    /**
     * 绘制所有的子View
     */
    fun drawItems(canvas: Canvas)

    /**
     * 对所有的子View 做变形操作
     */
    fun transForms()

    /**
     * 销毁所有的粒子
     */
    fun destroyAllView()

    /**
     * 开始粒子动画
     */
    fun startAnimation()

    /**
     * 停止粒子动画
     */
    fun stopAnimation()

    /**
     * 设置粒子适配器
     */
    fun setAdapter(adapter: ParticleAdapter)

    /**
     * 设置默认的刷新频率
     */
    fun setRenderTime(renderTime: Long)

    /**
     * 设置新增粒子的时间间隔
     */
    fun setIncreaseParticleInterval(intervalTime:Long)
    /**
     * 设置每次新增的粒子数目
     */
    fun setIncreaseParticleCount(count: Int)

    /**
     * 设置是否自动进行粒子动画  默认为True
     */
    fun setIsAutoPlay(isAuto:Boolean)

}