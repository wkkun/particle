package com.wkkun.particle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.wkkun.particle.base.BaseItem
import com.wkkun.particle.base.ParticleAdapter
import com.wkkun.particle.bubble.BubbleItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        particle.setAdapter(object : ParticleAdapter() {
            override fun newItem(parentWidth: Int, parentHeight: Int): BaseItem {
                return BubbleItem(parentWidth, parentHeight, this@MainActivity)
            }

            override fun preCreateCount(): Int {
                return 15
            }

        })
        particle.setIncreaseParticleInterval(300)
        particle.setIncreaseParticleCount(1)
    }


}
